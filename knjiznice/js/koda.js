
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var tempID;
var vneseni=0;

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
function klizGeneriranja() {
	
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
}

function generirajPodatke(stPacienta) {
	
	ehrId1 = "e339e7a2-c9a7-4360-a917-b7278b6e07e5";
	ehrId2 = "a58bb86e-9a29-47a2-bc43-719f9c412ab6";
	ehrId3 = "7a6b1f38-54c8-4b0d-b5a4-5da25ee2c24a";

	if (stPacienta == 1) {
			$("#preberiObstojeciVitalniZnak").append("option value="+ehrId1+"|"+30.20+">Dedek Mraz</option>");
		$("#preberiEhrIdZaVitalneZnake").append("option value="+ehrId1+">Dedek Mraz</option>");
	x
	}
	if (stPacienta == 2) {
		$("#preberiObstojeciVitalniZnak").append("option value="+ehrId2+"|"+36.50+">Matjaž Kralj</option>");
		$("#preberiEhrIdZaVitalneZnake").append("option value="+ehrId2+">Matjaž Kralj</option>");
		
	}
	if (stPacienta == 3) {
		
		$("#preberiObstojeciVitalniZnak").append("option value="+ehrId3+"|"+39.50+">Lucifer Seljak</option>");
		$("#preberiEhrIdZaVitalneZnake").append("option value="+ehrId3+">Lucifer Seljak</option>");
	}
}

$(document).ready(function() {
    
    $('#preberiEhrIdZaVitalneZnake').change(function() {
        $("#preberiMeritveVitalnihZnakovSporocilo").html("");
        $("meritveVitalnihZnakovEHRid").val($(this).html());
    })
    
})

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

// -------------------1.TABELA---------------------------------

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}
//------------vrnitev sporočila za prvo dejanje-------------

function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}


//------------------------------------2.tabela-----------------------------

function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra ="2018-05-27T11:40Z";
	var telesnaVisina = 180;
	var telesnaTeza = 70;
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = 120;
	var diastolicniKrvniTlak = 80;
	var nasicenostKrviSKisikom = 98;
	var merilec = "Janka Misica";

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

//--------------------------3.TABELA----------------------------------------
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();
	
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = "telesna temperatura";

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Zadnja vnešena telesna temperatura za bolnika <b>" + party.firstNames +" " + party.lastNames + "</b>.</span><br/><br/>");
				if (tip == "telesna temperatura") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
					    		vneseni=1;
					    		tempID = res[res.length-1].temperature;
					    		
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr><tr><td>" + res[res.length-1].time +
                          "</td><td class='text-right'>" + res[res.length-1].temperature +
                          " " + res[res.length-1].unit + "</td></tr></table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	

					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}  
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) 
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti 
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[1]);

	});

  /**
   * Napolni testni EHR ID 
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});


//------------------------gauge-----------

	 google.charts.load('current', {'packages':['gauge']});
	//var nastaviOb = google.charts.onclick(drawChart);
	
		function drawChart() {
	
			var temp = Math.floor(tempID);
		
			var odmik = 0;
	
			if (temp == 37 || temp == 35) {
				odmik = 50;
			}
			        	
			else if(temp > 37 || temp < 35) {
			 			odmik = 100;
			}
			
			else if (temp == 36) {
						odmik = 0;        
			}
	
			var data = google.visualization.arrayToDataTable([
			  ['Label', 'Value'],
			  ['Je nujno?', odmik]
			]);
			
			var options = {
			  width: 800, height: 240,
			  redFrom: 50, redTo: 100,
			  yellowFrom: 25 , yellowTo: 50,
			  minorTicks: 1
			};
	
			var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
	
			chart.draw(data, options);
		
		if (!vneseni) {
			$("#chart_div").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov! Prosim, vnesi podatke in poskusi znova.</span>");
		}
			
	}
	
	
